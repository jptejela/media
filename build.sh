#!/bin/bash
set -ex

rm -fr node_modules && \
npm install && \
npm run build